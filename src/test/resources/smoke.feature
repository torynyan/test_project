Feature: Smoke
  As a user
  I want to test all main site functionality
  So that I can be sure that site works correctly

  Scenario Outline: Check the name of the headline article
    Given User opens news page
    And User check the name of the headline article '<headline>'

    Examples:
      | headline                           |
      | A dozen nations tell citizens: leave Ukraine now|

  Scenario Outline: Checks Secondary Article Titles
    Given User opens news page
    And User check the name of the secondary articles <secArticles>

    Examples:
      | secArticles                  |
      |Deal to avoid war in Ukraine within reach - Macron,N Korea funding missile projects with stolen crypto,Top firms exaggerating climate progress - study,The fast fashion graveyard in a desert,India bids farewell to iconic singer Lata Mangeshkar,Charles leads Jubilee tributes to 'remarkable' Queen,Tunisia's top judges turn against president,Senegal win Africa Cup of Nations for first time,Whole villages swept away by Madagascar cyclone,Senegal win Africa Cup of Nations for first time,Whole villages swept away by Madagascar cyclone,British serial killer 'claims 1996 double murder',Tragic end for boy trapped in Moroccan well,African leaders complain about 'wave of coups' |
