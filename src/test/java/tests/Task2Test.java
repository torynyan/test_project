package tests;

import driver_manager.PageFactoryManager;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.SportPage;

import static org.testng.Assert.assertEquals;

public class Task2Test extends BaseTest {


    @DataProvider(name = "team-list")
    public Object[][] dpTeamMethod() {
        return new Object[][]{{0, "Ayr United", "Dunfermline", "1", "1"},
                {2, "Inverness Caledonian Thistle", "Greenock Morton", "0", "1"},
                {6, "Raith Rovers", "Hamilton Academical", "0", "0"},
                {8, "Arbroath", "Kilmarnock", "1", "0"},
                {10, "Partick Thistle", "Ayr United", "1", "0"}};
    }

    @Test(dataProvider = "team-list")
    public void checkThatTeamScoresDisplayCorrectly1(int indOfNT, String team1, String team2, String score1, String score2) {

        PageFactoryManager.sportPage.goToHomePage()
                .clickOnSportButton()
                .clickOnMenuBarFootball()
                .clickOnScoresAndFix()
                .findChampionship("Scottish Championship")
                .clickOnResultsForAMonth()
                .clickOnNameOfTeam(indOfNT);


        assertEquals(PageFactoryManager.sportPage.getNameOfTeamInCenterText(0), team1);
        assertEquals(PageFactoryManager.sportPage.getScoreOfTeamInCenterText(0), score1);
        assertEquals(PageFactoryManager.sportPage.getNameOfTeamInCenterText(1), team2);
        assertEquals(PageFactoryManager.sportPage.getScoreOfTeamInCenterText(1), score2);

    }
}
