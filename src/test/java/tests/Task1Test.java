package tests;

import driver_manager.PageFactoryManager;
import org.testng.annotations.Test;
import pages.NewsPage;

import java.util.Arrays;
import java.util.List;

import static org.testng.Assert.assertEquals;

public class Task1Test extends BaseTest{


    @Test
    public void checkTheNameOfTheHeadlineArticle(){
        PageFactoryManager.newsPage.goToHomePage()
               .clickOnNewsButton();
        assertEquals(PageFactoryManager.newsPage.getHeadlineArticleText(),("Ottawa declares emergency over trucker protests"));
    }

    @Test
    public void checksSecondaryArticleTitles(){
        List<String> expectedList = Arrays.asList(
                "Deal to avoid war in Ukraine within reach - Macron",
                "N Korea funding missile projects with stolen crypto",
                "Top firms exaggerating climate progress - study",
                "The fast fashion graveyard in a desert",
                "India bids farewell to iconic singer Lata Mangeshkar",
                "Charles leads Jubilee tributes to 'remarkable' Queen",
                "Tunisia's top judges turn against president",
                "Senegal win Africa Cup of Nations for first time",
                "Whole villages swept away by Madagascar cyclone",
                "Senegal win Africa Cup of Nations for first time",
                "Whole villages swept away by Madagascar cyclone",
                "British serial killer 'claims 1996 double murder'",
                "Tragic end for boy trapped in Moroccan well",
                "African leaders complain about 'wave of coups'"
                );

        PageFactoryManager.newsPage.goToHomePage()
                .clickOnNewsButton();

        for (int i = 0; i < expectedList.size(); i++) {
            assertEquals(PageFactoryManager.newsPage.getSecondaryArticleTitles(i),expectedList.get(i));
        }
    }

}
