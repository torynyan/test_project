package stepdefinitions;

import driver_manager.DriverManager;
import driver_manager.PageFactoryManager;
import io.cucumber.java.en.And;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import tests.BaseTest;

import java.util.List;

import static io.github.bonigarcia.wdm.WebDriverManager.chromedriver;
import static org.testng.Assert.assertEquals;

public class StepDefinitions {
    protected WebDriver driver = DriverManager.getDriver();

    @Before
    public void profileSetUp() {
        chromedriver().setup();

    }
    @And("User opens news page")
    public void openNewsPage() {
        PageFactoryManager.newsPage.goToHomePage().clickOnNewsButton();
    }


    @And("User check the name of the headline article {string}")
    public void userCheckTheNameOfTheHeadlineArticle(String headline) {
        assertEquals(PageFactoryManager.newsPage.getHeadlineArticleText(),(headline));
    }

    @After
    public void tearDown() {
        DriverManager.closeDriver();
    }

    @And("User check the name of the secondary articles (.*)")
    public void userCheckTheNameOfTheSecondaryArticlesSecArticles(List<String> secArticles) {
        for (int i = 0; i < secArticles.size(); i++) {
            assertEquals(PageFactoryManager.newsPage.getSecondaryArticleTitles(i),secArticles.get(i));
        }
    }
}
