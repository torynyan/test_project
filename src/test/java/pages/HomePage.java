package pages;

import java.time.Duration;

public class HomePage extends BasePage{

    public void clickOnNewsButton(){
        findByXpath("//header//li[@class='orb-nav-newsdotcom']",
                Duration.ofSeconds(DEFAULT_TIMEOUT)).click();
        new NewsPage();
    }

    public SportPage clickOnSportButton(){
        findByXpath("//header//li[@class='orb-nav-sport']",
                Duration.ofSeconds(DEFAULT_TIMEOUT)).click();
        return new SportPage();
    }
}
