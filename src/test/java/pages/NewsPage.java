package pages;

import java.time.Duration;

public class NewsPage extends BasePage{

    public String getHeadlineArticleText(){
        return findByXpath("//div[@class='gs-c-promo-body gs-u-display-none gs-u-display-inline-block@m gs-u-mt@xs gs-u-mt0@m gel-1/3@m']//h3",
                Duration.ofSeconds(DEFAULT_TIMEOUT)).getText();
    }

    public String getSecondaryArticleTitles(int ind){
        return findByXpathList("//div[@id='news-top-stories-container']//a[@class='gs-c-promo-heading gs-o-faux-block-link__overlay-link gel-pica-bold nw-o-link-split__anchor']//h3[@class='gs-c-promo-heading__title gel-pica-bold nw-o-link-split__text']",
                Duration.ofSeconds(DEFAULT_TIMEOUT)).get(ind).getText();
    }
}

