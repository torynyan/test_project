package pages;

import java.time.Duration;

public class SportPage extends BasePage{

    public SportPage clickOnMenuBarFootball(){
        findByXpath("//div[@role='menubar']//a[@data-stat-title='Football']",
                Duration.ofSeconds(DEFAULT_TIMEOUT)).click();
        return this;
    }

    public SportPage clickOnScoresAndFix(){
        findByXpath("//div[@class='gel-wrap']//a[@data-stat-title='Scores & Fixtures']",
                Duration.ofSeconds(DEFAULT_TIMEOUT)).click();
        return this;
    }

    public SportPage findChampionship(String keyword){
        findByXpath("//input[@class='sp-c-search__input gel-1/1']",
                Duration.ofSeconds(DEFAULT_TIMEOUT)).sendKeys(keyword);
        findByXpathList("//a[@class='sp-c-search__result-item']",
                Duration.ofSeconds(DEFAULT_TIMEOUT)).get(0).click();
        return this;
    }

    public SportPage clickOnResultsForAMonth(){
        findByXpath("//a[contains(@href,'filter=results')]",
                Duration.ofSeconds(DEFAULT_TIMEOUT)).click();
        return this;
    }
    public SportPage clickOnNameOfTeam(int ind){
        findByXpathList("//ul[contains(@class,'gs-o-list-ui')]//span[contains(@class,'gs-u-display-none')]",
                Duration.ofSeconds(DEFAULT_TIMEOUT)).get(ind).click();
        return this;
    }

    public String getNameOfTeamInCenterText(int ind){
        return findByXpathList("//div[@class='football-oppm-header']//span[contains(@class,'gs-u-display-none')]",
                Duration.ofSeconds(DEFAULT_TIMEOUT)).get(ind).getText();
    }

    public String getScoreOfTeamInCenterText(int ind){
        return findByXpathList("//div[@class='football-oppm-header']//span[contains(@class,'sp-c-fixture__number')]",
                Duration.ofSeconds(DEFAULT_TIMEOUT)).get(ind).getText();
    }
}
