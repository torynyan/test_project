package driver_manager;

import pages.NewsPage;
import pages.SportPage;

public class PageFactoryManager {

    public static final NewsPage newsPage = new NewsPage();

    public static final SportPage sportPage = new SportPage();

}
